#pragma once
#include "Piece.h"
#define PREVIOUS -1
#define TWO_STEPS 2

class Knight : public Piece
{
public:
	virtual bool isPathFree(const int dest[], Piece*** boardArr);
	Knight(int x, int y, bool side) : Piece(x, y, side, 'n') {};
	~Knight();
};