#include "Pawn.h"

Pawn::~Pawn()
{
}

bool Pawn::canEat(const int dest[], Piece*** boardArr) const
{
	int source[NUM_OF_AXISES] = { this->getX(), this->getY() };
	bool rValue = false;
	int nearbyArr[ROW][NUM_OF_AXISES] = { {PREVIOUS, 1 }, {1,1} };
	if (boardArr[dest[X]][dest[Y]])
	{
		if (boardArr[source[X]][source[Y]]->getSide() == WHITE)
		{
			if (source[X] + nearbyArr[0][X] == dest[X] && source[Y] + nearbyArr[0][Y] == dest[Y] ||
				source[X] + nearbyArr[1][X] == dest[X] && source[Y] + nearbyArr[1][Y] == dest[Y])
			{
				rValue = true;
			}
		}
		else
		{
			if (source[X] + nearbyArr[0][X] == dest[X] && source[Y] - nearbyArr[0][Y] == dest[Y] ||
				source[X] + nearbyArr[1][X] == dest[X] && source[Y] - nearbyArr[1][Y] == dest[Y])
			{
				rValue = true;
			}
		}
	}
	return rValue;
}

bool Pawn::isPathFree(const int dest[], Piece*** boardArr)
{
	bool ans = true;
	unsigned int i = 0;
	int source[NUM_OF_AXISES] = { this->getX(), this->getY() };
	int xDiff = source[X] - dest[X];
	int yDiff = source[Y] - dest[Y];
	int smaller = 0;

	if (!xDiff && (boardArr[source[X]][source[Y]]->getSide() && source[Y] > dest[Y] || !boardArr[source[X]][source[Y]]->getSide() && source[Y] < dest[Y])  && ((int)abs(yDiff) == 1  || //check the diraction of the move is legal
		(int)abs(yDiff) == 2 && (source[Y] == 1 && boardArr[source[X]][source[Y]]->getSide() == WHITE || source[Y] == 6 && boardArr[source[X]][source[Y]]->getSide() == BLACK))) //check conditions for 2 steps
	{
		if (yDiff < 0)
		{
			smaller = source[Y];
		}
		else
		{
			smaller = dest[Y];
		}
		for (i = 1; i < (int)abs(yDiff); i++)
		{
			if (boardArr[source[X]][smaller + i])//check if a piece is already in the spot
			{
				ans = false;
				break;
			}
		}
	}
	else if (!this->canEat(dest, boardArr))
	{
		ans = false;
	}
	return ans;
}

