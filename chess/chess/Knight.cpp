#include "Knight.h"

Knight::~Knight()
{
}

bool Knight::isPathFree(const int dest[], Piece*** boardArr)
{
	bool ans = true;
	unsigned int i = 0;
	int source[NUM_OF_AXISES] = { this->getX(), this->getY() };
	bool canReachDest = false;
	int nearbyArr[ROW][NUM_OF_AXISES] = { {PREVIOUS, -TWO_STEPS }, { -TWO_STEPS, PREVIOUS}, { -TWO_STEPS, 1}, { PREVIOUS, TWO_STEPS }, {1, TWO_STEPS}, {TWO_STEPS, 1 }, {TWO_STEPS, PREVIOUS}, {1, -TWO_STEPS} };
	for (i = 0; i < ROW; i++)
	{
		if (source[X] + nearbyArr[i][X] == dest[X] && source[Y] + nearbyArr[i][Y] == dest[Y])
		{
			canReachDest = true;
		}
	}
	if (!canReachDest)
	{
		ans = false;
	}
	return ans;
}
