#pragma once
#include "Piece.h"


class Rook : public Piece
{
public:
	virtual bool isPathFree(const int dest[], Piece*** boardArr);
	Rook(int x, int y, bool side) : Piece(x, y, side, 'r') {};
	~Rook();
};