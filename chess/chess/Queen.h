#pragma once
#include "Piece.h"


class Queen : public Piece
{
public:
	virtual bool isPathFree(const int dest[], Piece*** boardArr);
	Queen(int x, int y, bool side) : Piece(x, y, side, 'q') {};
	~Queen();
};