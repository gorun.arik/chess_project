#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"
#define NORMAL_CHESS_STR "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"


using namespace std;
void main()
{
	srand(time_t(NULL));
	unsigned int i = 0;
	unsigned int j = 0;
	unsigned short answer = 0;
	Board gameBoard("a1a1", WHITE);
	string boardStr = "";

	Pipe p;
	bool isConnect = p.connect();

	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}

	
	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, NORMAL_CHESS_STR); // just example...

	p.sendMessageToGraphics(msgToGraphics);   // send the board string

											  // get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)

		gameBoard.setIndexes(msgFromGraphics);
		
		answer = gameBoard.answer();
		
		boardStr = gameBoard.createBoardString();
		for (i = 0; i < 8; i++)
		{
			for (j = 0; j < 8; j++)
			{
				std::cout << boardStr[i * 8 + j] << ' ';
			}
			std::cout << endl;
		}
		strcpy_s(msgToGraphics, std::to_string(answer).c_str()); // msgToGraphics should contain the result of the operation
											  // return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}