#include "Queen.h"

Queen::~Queen()
{
}

bool Queen::isPathFree(const int dest[], Piece*** boardArr)
{
	bool ans = true;
	unsigned int i = 0;
	int source[NUM_OF_AXISES] = { this->getX(), this->getY() };
	int xDiff = source[X] - dest[X];
	int yDiff = source[Y] - dest[Y];
	int smallerX = 0;
	int smallerY = 0;
	int xOfSmallerY = 0;

	if (yDiff < 0)
	{
		smallerY = source[Y];
		xOfSmallerY = source[X];
	}
	else
	{
		smallerY = dest[Y];
		xOfSmallerY = dest[X];
	}
	if (xDiff < 0)
	{
		smallerX = source[X];
	}
	else
	{
		smallerX = dest[X];
	}
	if ((int)abs(xDiff) == (int)abs(yDiff))
	{
		if (smallerX == source[X] && smallerY == source[Y] || smallerX == dest[X] && smallerY == dest[Y])
		{
			for (i = 1; i < (int)abs(yDiff); i++)
			{
				if (boardArr[xOfSmallerY + i][smallerY + i])//check if a piece is already in the spot
				{
					ans = false;
					break;
				}
			}
		}
		else
		{
			for (i = 1; i < (int)abs(yDiff); i++)
			{
				if (boardArr[xOfSmallerY - i][smallerY + i])//check if a piece is already in the spot
				{
					ans = false;
					break;
				}
			}
		}
	}
	else if (!xDiff)
	{
		for (i = 1; i < (int)abs(yDiff); i++)
		{
			if (boardArr[source[X]][smallerY + i])//check if a piece is already in the spot
			{
				ans = false;
				break;
			}
		}
	}
	else if (!yDiff)
	{
		for (i = 1; i < (int)abs(xDiff); i++)
		{
			if (boardArr[smallerX + i][source[1]])//check if a piece is already in the spot
			{
				ans = false;
				break;
			}
		}
	}
	else
	{
		ans = false;
	}
	return ans;
}