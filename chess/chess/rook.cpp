#include "Rook.h"

Rook::~Rook()
{
}

bool Rook::isPathFree(const int dest[], Piece*** boardArr)
{
	bool ans = true;
	unsigned int i = 0;
	int source[NUM_OF_AXISES] = { this->getX(), this->getY() };
	int xDiff = source[X] - dest[X];
	int yDiff = source[Y] - dest[Y];
	int smaller = 0;

	if (!xDiff)
	{
		if (yDiff < 0)
		{
			smaller = source[Y];
		}
		else
		{
			smaller = dest[Y];
		}
		for (i = 1; i < (int)abs(yDiff); i++)
		{
			if (boardArr[source[X]][smaller + i])//check if a piece is already in the spot
			{
				ans = false;
				break;
			}
		}
	}
	else if(!yDiff)
	{
		if (xDiff < 0)
		{
			smaller = source[X];
		}
		else
		{
			smaller = dest[X];
		}
		for (i = 1; i < (int)abs(xDiff); i++)
		{
			if (boardArr[smaller + i][source[Y]])//check if a piece is already in the spot
			{
				ans = false;
				break;
			}
		}
	}
	else
	{
		ans = false;
	}
	return ans;
}