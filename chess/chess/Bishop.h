#pragma once
#include "Piece.h"


class Bishop : public Piece
{
public:
	virtual bool isPathFree(const int dest[], Piece*** boardArr);
	Bishop(int x, int y, bool side) : Piece(x, y, side, 'b') {};
	~Bishop();
};
