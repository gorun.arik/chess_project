#include "Piece.h"

Piece::Piece(int x, int y, bool side, char type)
{
	this->setSide(side);
	this->setX(x);
	this->setY(y);
	if (!this->getSide())
	{
		this->setType(type-DIFFERENCE_IN_CASING);
	}
	else
	{
		this->setType(type);
	}
}

Piece::~Piece()
{
}

void Piece::setX(const int x)
{
	this->_x = x;
}

void Piece::setY(const int y)
{
	this->_y = y;
}

void Piece::setSide(const bool side)
{
	this->_side = side;
}

void Piece::setType(const char type)
{
	this->_type = type;
}

int Piece::getX() const
{
	return this->_x;
}

int Piece::getY() const
{
	return this->_y;
}

bool Piece::getSide() const
{
	return this->_side;
}

char Piece::getType() const
{
	return this->_type;
}
