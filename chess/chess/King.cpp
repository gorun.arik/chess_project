#include "King.h"

King::~King()
{
}

bool King::isPathFree(const int dest[], Piece*** boardArr)
{
	bool ans = true;
	unsigned int i = 0;
	int source[NUM_OF_AXISES] = { this->getX(), this->getY() };
	bool canReachDest = false;
	int nearbyArr[ROW][NUM_OF_AXISES] = { {PREVIOUS, PREVIOUS }, { PREVIOUS, 0}, { PREVIOUS, 1}, {0, PREVIOUS }, {0, 1}, {1, PREVIOUS }, {1, 0}, {1, 1} };
	for (i = 0; i < ROW; i++)
	{
		if (source[X] + nearbyArr[i][X] == dest[X] && source[Y] + nearbyArr[i][Y] == dest[Y])
		{
			canReachDest = true;
		}
	}
	if (!canReachDest)
	{
		ans = false;
	}
	return ans;
}
