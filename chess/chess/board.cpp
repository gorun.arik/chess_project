#include "Board.h"
#include "Rook.h"
#include "King.h"
#include "Bishop.h"
#include "Queen.h"
#include "Knight.h"
#include "Pawn.h"
Board::Board( const string indexesToChange, bool turn)
{
	unsigned int i = 0;
	unsigned int j = 0;
	this->_turn = WHITE;
	this->setIndexes(indexesToChange);
	int FourPiecesArr[TWO_PAIRS][NUM_OF_AXISES] = { {0, 0}, { 0,ROW-1 }, { ROW-1, 0 }, {ROW-1,ROW-1} };
	int PawnsArr[2 * ROW][NUM_OF_AXISES] = { { 0, 1 },{ 1, 1 },{ 2, 1 },{ 3, 1 },{ 4, 1 },{ 5, 1 },{ 6, 1 },{ 7, 1 },{ 0, 6 },{ 1, 6 },{ 2, 6 },{ 3, 6 },{ 4, 6 },{ 5, 6 },{ 6, 6 },{ 7, 6 }};
	this->_board = new Piece**[ROW];
	bool currColor = WHITE;

	for (i = 0; i < ROW; i++)
	{
		this->_board[i] = new Piece*[ROW];
		for (j = 0; j < ROW; j++)
		{
			this->_board[i][j] = nullptr;
		}
	}

	this->_board[3][ROW - 1] = new King(3, ROW - 1, BLACK);
	this->_board[3][0] = new King(3, 0, WHITE);
	this->_board[4][ROW - 1] = new Queen(4, ROW - 1, BLACK);
	this->_board[4][0] = new Queen(4, 0, WHITE);
	for (i = 0; i < TWO_PAIRS; i++)
	{
		this->_board[FourPiecesArr[i][X]][FourPiecesArr[i][Y]] = new Rook(FourPiecesArr[i][X], FourPiecesArr[i][Y], currColor);
		currColor = !currColor;
	}
	for (i = 0; i < TWO_PAIRS; i++)
	{
		if (i < 2)
		{
			this->_board[FourPiecesArr[i][X] + 1][FourPiecesArr[i][Y]] = new Knight(FourPiecesArr[i][X] + 1, FourPiecesArr[i][Y], currColor);
		}
		else
		{
			this->_board[FourPiecesArr[i][X] - 1][FourPiecesArr[i][Y]] = new Knight(FourPiecesArr[i][X] - 1, FourPiecesArr[i][Y], currColor);
		}		
		currColor = !currColor;
	}
	for (i = 0; i < TWO_PAIRS; i++)
	{
		if (i < 2)
		{
			this->_board[FourPiecesArr[i][X] + 2][FourPiecesArr[i][Y]] = new Bishop(FourPiecesArr[i][X] + 2, FourPiecesArr[i][Y], currColor);
		}
		else
		{
			this->_board[FourPiecesArr[i][X] - 2][FourPiecesArr[i][Y]] = new Bishop(FourPiecesArr[i][X] - 2, FourPiecesArr[i][Y], currColor);
		}
		currColor = !currColor;
	}
	for (i = 0; i < 2 * ROW; i++)
	{
		if (i < ROW)
		{
			this->_board[PawnsArr[i][X]][PawnsArr[i][Y]] = new Pawn(PawnsArr[i][X], PawnsArr[i][Y], WHITE);
		}
		else
		{
			this->_board[PawnsArr[i][X]][PawnsArr[i][Y]] = new Pawn(PawnsArr[i][X], PawnsArr[i][Y], BLACK);
		}
	}
}

Board::~Board()
{
	unsigned int i = 0;
	unsigned int j = 0;
	for (i = 0; i < ROW; i++)
	{
		for (j = 0; j < ROW; j++)
		{
			delete this->_board[i][j];
		}
		delete[] this->_board[i];
	}
	delete[] this->_board;
}

int** Board::calcIndexes() const
{
	unsigned int i = 0;
	int** indexes = new int* [NUM_OF_AXISES];
	for (i = 0; i < NUM_OF_AXISES; i++)
	{
		indexes[i] = new int[NUM_OF_AXISES];
	}
	string index = this->getIndexes();
	unsigned int first = 0;
	int second = 0;
	for (i = 1; i <= NUM_OF_AXISES; i++)
	{
		switch (index[NUM_OF_AXISES*i - NUM_OF_AXISES])
		{
		case 'a': first = 0;
			break;
		case 'b': first = 1;
			break;
		case 'c': first = 2;
			break;
		case 'd': first = 3;
			break;
		case 'e': first = 4;
			break;
		case 'f': first = 5;
			break;
		case 'g': first = 6;
			break;
		case 'h': first = 7;
			break;
		}
		second = atoi(&index[NUM_OF_AXISES*i-1]) - 1;
		indexes[i - 1][X] = first;
		indexes[i - 1][Y] = second;
	}
	return indexes;
}

void Board::setIndexes(const string indexesToChange)
{
	this->_indexesToChange = indexesToChange;
}

void Board::setTurn(const bool turn)
{
	this->_turn = turn;
}

void Board::swapTurns()
{
	this->setTurn(!this->getTurn());
}

string Board::getIndexes() const
{
	return this->_indexesToChange;
}

unsigned short Board::getTurn() const
{
	return this->_turn;
}

string Board::createBoardString()
{
	string boardStr = "";
	short i = 0;
	unsigned short j = 0;
	for (i = ROW-1; i >= 0; i--)
	{
		for (j = 0; j < ROW; j++)
		{
			if (!this->_board[j][i])
			{
				boardStr += '#';
			}
			else
			{
				boardStr += this->_board[j][i]->getType();
			}
		}
	}
	boardStr += '0';
	return boardStr;
}


bool Board::isSourceValid(const int source[]) const
{
	bool rValue = true;
	if (!this->_board[source[X]][source[Y]] || this->_board[source[X]][source[Y]]->getSide() != this->getTurn()) //check if source's and current turn's colors are the same
	{
		rValue = false;
	}
	return rValue;
}

bool Board::areIndexesTheSame(const int source[], const int dest[]) const
{
	return source[X] == dest[X] && source[Y] == dest[Y];
}

bool Board::isDestValid(const int source[], const int dest[]) const
{
	bool rValue = true;
	if (this->_board[dest[X]][dest[Y]] && ((this->_board[source[X]][source[Y]]->getType() == 'p' || this->_board[source[X]][source[Y]]->getType() == 'P') 
		|| this->_board[dest[X]][dest[Y]]->getSide() == this->getTurn()))
	{
		rValue = false;
	}
	return rValue;
}

unsigned short Board::answer()
{
	unsigned short answerForFrontEnd = 0;
	unsigned int i = 0;
	unsigned int j = 0;
	Piece* enemyKing = 0;
	Piece* selfKing = 0;
	int** indexes = this->calcIndexes();
	int source[NUM_OF_AXISES] = { indexes[0][X], indexes[0][Y] };
	int dest[NUM_OF_AXISES] = { indexes[1][X], indexes[1][Y] };
	for (i = 0; i < NUM_OF_AXISES; i++)
	{
		delete[] indexes[i];
	}
	if (source[X] < 0 || source[X] > ROW - 1 || source[Y] < 0 || source[Y] > ROW - 1 || dest[X] < 0 || dest[X] > ROW - 1 || dest[Y] < 0 || dest[Y] > ROW - 1)
	{
		answerForFrontEnd = INDEXES_ERR;
	}
	else if (!this->isSourceValid(source))
	{
		answerForFrontEnd = SOURCE_ERR;
	}
	else if (this->areIndexesTheSame(source, dest))
	{
		answerForFrontEnd = SAME_SQUARE_ERR;
	}
	else if (!this->isDestValid(source, dest))
	{
		answerForFrontEnd = DEST_ERR;
	}
	else if (!(this->_board[source[X]][source[Y]]->isPathFree(dest, this->_board)))
	{
		answerForFrontEnd = ILEGAL_MOVE_ERR;
	}
	else
	{
		this->swapIndexesToChange(true);
		for (i = 0; i < ROW; i++)
		{
			for (j = 0; j < ROW; j++)
			{
				if (this->_board[i][j] && (this->_board[i][j]->getType() == 'k' || this->_board[i][j]->getType() == 'K'))
				{
					if (this->_board[i][j]->getSide() != this->getTurn())
					{
						enemyKing = this->_board[i][j];
					}
					else
					{
						selfKing = this->_board[i][j];
					}
				}
			}
		}
		if (this->isCheck(selfKing))
		{
			answerForFrontEnd = SELF_CHECK_ERR;
			this->swapIndexesToChange(false);
		}
		else if (this->isCheck(enemyKing))
		{
			if (this->isCheckmate(enemyKing))
			{
				answerForFrontEnd = 8;
			}
			else
			{
				answerForFrontEnd = MADE_CHECK;
			}
		}
	}
	if (answerForFrontEnd == 0 || answerForFrontEnd == MADE_CHECK)
	{
		this->swapTurns();
	}
	delete[] indexes;
	return answerForFrontEnd;
}

unsigned short Board::answer(const int source[], const int dest[])
{
	unsigned short answerForFrontEnd = 0;
	unsigned int i = 0;
	unsigned int j = 0;
	Piece* enemyKing = 0;
	Piece* selfKing = 0;
	
	if (source[X] < 0 || source[X] > ROW - 1 || source[Y] < 0 || source[Y] > ROW - 1 || dest[X] < 0 || dest[X] > ROW - 1 || dest[Y] < 0 || dest[Y] > ROW - 1)
	{
		answerForFrontEnd = INDEXES_ERR;
	}
	else if (!this->isSourceValid(source))
	{
		answerForFrontEnd = SOURCE_ERR;
	}
	else if (this->areIndexesTheSame(source, dest))
	{
		answerForFrontEnd = SAME_SQUARE_ERR;
	}
	else if (!this->isDestValid(source, dest))
	{
		answerForFrontEnd = DEST_ERR;
	}
	else if (!(this->_board[source[X]][source[Y]]->isPathFree(dest, this->_board)))
	{
		answerForFrontEnd = ILEGAL_MOVE_ERR;
	}
	else
	{
		this->swapIndexesToChange(false, source, dest);
		for (i = 0; i < ROW; i++)
		{
			for (j = 0; j < ROW; j++)
			{
				if (this->_board[i][j] && (this->_board[i][j]->getType() == 'k' || this->_board[i][j]->getType() == 'K'))
				{
					if (this->_board[i][j]->getSide() != this->getTurn())
					{
						enemyKing = this->_board[i][j];
					}
					else
					{
						selfKing = this->_board[i][j];
					}
				}
			}
		}
		if (this->isCheck(selfKing))
		{
			answerForFrontEnd = SELF_CHECK_ERR;
		}
		this->swapIndexesToChange(false, source, dest);
	}
	return answerForFrontEnd;
}

bool Board::isCheck(Piece*& king) const
{
	unsigned short i = 0, j = 0;
	int dest[NUM_OF_AXISES] = { king->getX(), king->getY() };
	int source[NUM_OF_AXISES] = { 0 };

	for (i; i < ROW; i++)
	{
		for(j = 0; j < ROW; j++)
		{
			if (this->_board[i][j])
			{
				source[0] = i;
				source[1] = j;
				if (king->getSide() != this->_board[i][j]->getSide())
				{
					if (this->_board[i][j]->isPathFree(dest, this->_board))
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

bool Board::isEating() const
{
	unsigned int i = 0;
	int** indexes = this->calcIndexes();
	int dest[NUM_OF_AXISES] = { indexes[1][X], indexes[1][Y] };
	for (i = 0; i < NUM_OF_AXISES; i++)
	{
		delete[] indexes[i];
	}
	delete[] indexes;
	return this->_board[dest[X]][dest[Y]];
}

void Board::swapIndexesToChange(bool checkEating) const
{
	unsigned int i = 0;
	int** indexes = this->calcIndexes();
	int source[NUM_OF_AXISES] = { indexes[0][X], indexes[0][Y] };
	int dest[NUM_OF_AXISES] = { indexes[1][X], indexes[1][Y] };
	for (i = 0; i < NUM_OF_AXISES; i++)
	{
		delete[] indexes[i];
	}
	if (checkEating && this->isEating())
	{
		this->_board[dest[X]][dest[Y]] = nullptr;
	}
	Piece* temp = this->_board[source[X]][source[Y]];
	this->_board[source[X]][source[Y]] = this->_board[dest[X]][dest[Y]];
	if (this->_board[source[X]][source[Y]])
	{
		this->_board[source[X]][source[Y]]->setX(source[X]);
		this->_board[source[X]][source[Y]]->setY(source[Y]);
	}
	this->_board[dest[X]][dest[Y]] = temp;
	if (this->_board[dest[X]][dest[Y]])
	{
		this->_board[dest[X]][dest[Y]]->setX(dest[X]);
		this->_board[dest[X]][dest[Y]]->setY(dest[Y]);
	}
	delete[] indexes;
}

void Board::swapIndexesToChange(const bool checkEating, const int source[], const int dest[]) const
{
	unsigned int i = 0;
	if (checkEating && this->isEating())
	{
		this->_board[dest[X]][dest[Y]] = nullptr;
	}
	Piece* temp = this->_board[source[X]][source[Y]];
	this->_board[source[X]][source[Y]] = this->_board[dest[X]][dest[Y]];
	if (this->_board[source[X]][source[Y]])
	{
		this->_board[source[X]][source[Y]]->setX(source[X]);
		this->_board[source[X]][source[Y]]->setY(source[Y]);
	}
	this->_board[dest[X]][dest[Y]] = temp;
	if (this->_board[dest[X]][dest[Y]])
	{
		this->_board[dest[X]][dest[Y]]->setX(dest[X]);
		this->_board[dest[X]][dest[Y]]->setY(dest[Y]);
	}
}


bool Board::isCheckmate(Piece*& king)
{
	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int k = 0;
	int sourceArr[NUM_OF_AXISES] = { 0 };
	int destArr[NUM_OF_AXISES] = { 0 };
	this->swapTurns();
	for (i = 0; i < ROW; i++)
	{
		for (j = 0; j < ROW; j++)
		{
			if (this->_board[i][j] && this->_board[i][j]->getSide() == this->getTurn())
			{
				sourceArr[X] = i;
				sourceArr[Y] = j;
				for (k = 0; k < ROW*ROW; k++)
				{
					destArr[X] = (int)(k / ROW);
					destArr[Y] = k % ROW;
					if (this->answer(sourceArr, destArr) == 0)
					{
						this->swapTurns();
						return false;
					}
				}
			}
		}
	}
	this->swapTurns();
	return true;
}