#pragma once
#include "Piece.h"
#include "stdafx.h"
#define NUM_OF_AXISES 2
#define TWO_PAIRS 4
#define ROW 8
#define MADE_CHECK 1
#define SOURCE_ERR 2
#define DEST_ERR 3
#define SELF_CHECK_ERR 4
#define INDEXES_ERR 5
#define ILEGAL_MOVE_ERR 6
#define SAME_SQUARE_ERR 7
#define X 0
#define Y 1

using std::string;

class Piece;
class Board
{
	string _indexesToChange;
	bool _turn;
	int** calcIndexes() const;
	bool isSourceValid(const int source[]) const;
	bool areIndexesTheSame(const int source[], const int dest[]) const;
	bool isDestValid(const int source[], const int dest[]) const;
	bool isCheck(Piece*& king) const;
	unsigned short answer(const int source[], const int dest[]);
	bool isCheckmate(Piece*& king);
	bool isEating() const;
	void swapIndexesToChange(const bool checkEating) const;
	void swapIndexesToChange(const bool checkEating, const int source[], const int dest[]) const;
	void swapTurns();
public:
	Piece *** _board;
	Board(const string indexesToChange, const bool turn);
	~Board();
	unsigned short answer();
	string createBoardString();
	//setters
	void setIndexes(const string indexesToChange);
	void setTurn(const bool turn);
	//getters
	string getIndexes() const;
	unsigned short getTurn() const;
};