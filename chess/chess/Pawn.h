#pragma once
#include "Piece.h"
#define PREVIOUS -1

class Pawn : public Piece
{
	bool canEat(const int dest[], Piece*** boardArr) const;
public:
	virtual bool isPathFree(const int dest[], Piece*** boardArr);
	Pawn(int x, int y, bool side) : Piece(x, y, side, 'p') {};
	~Pawn();
};
