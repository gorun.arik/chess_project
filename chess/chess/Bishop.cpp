#include "Bishop.h"

Bishop::~Bishop()
{
}

bool Bishop::isPathFree(const int dest[], Piece*** boardArr)
{
	bool ans = true;
	unsigned int i = 0;
	int source[NUM_OF_AXISES] = { this->getX(), this->getY() };
	int xDiff = source[X] - dest[X];
	int yDiff = source[Y] - dest[Y];
	int smallerY = 0;
	int xOfSmallerY = 0;

	if ((int)abs(xDiff) == (int)abs(yDiff))
	{
		if (yDiff < 0)
		{
			smallerY = source[Y];
			xOfSmallerY = source[X];
		}
		else
		{
			smallerY = dest[Y];
			xOfSmallerY = dest[X];
		}
		if (xDiff < 0 && smallerY == source[Y] || xDiff > 0 && smallerY == dest[Y])
		{
			for (i = 1; i < (int)abs(yDiff); i++)
			{
				if (boardArr[xOfSmallerY + i][smallerY + i])//check if a piece is already in the spot
				{
					ans = false;
					break;
				}
			}
		}
		else
		{
			for (i = 1; i < (int)abs(yDiff); i++)
			{
				if (boardArr[xOfSmallerY - i][smallerY + i])//check if a piece is already in the spot
				{
					ans = false;
					break;
				}
			}
		}
	}
	else
	{
		ans = false;
	}
	return ans;
}