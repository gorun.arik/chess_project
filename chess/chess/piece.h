#pragma once
#include "Board.h"
#define WHITE 0
#define BLACK !WHITE
#define DIFFERENCE_IN_CASING 32

class Piece
{
protected:
	int _x;
	int _y;
	bool _side;
	char _type;
public:
	Piece(int x, int y, bool side, char type);
	~Piece();
	virtual bool isPathFree(const int dest[], Piece*** boardArr) = 0;
	//setters
	void setX(const int x);
	void setY(const int y);
	void setSide(const bool side);
	void setType(const char type);
	//getters
	int getX() const;
	int getY() const;
	bool getSide() const;
	char getType() const;
};