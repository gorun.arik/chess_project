#pragma once
#include "Piece.h"
#define PREVIOUS -1

class King : public Piece
{
public:
	virtual bool isPathFree(const int dest[], Piece*** boardArr);
	King(int x, int y, bool side) : Piece(x, y, side, 'k') {};
	~King();
};
